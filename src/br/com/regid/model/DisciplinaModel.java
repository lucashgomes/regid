package br.com.regid.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="disciplina")
public class DisciplinaModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) //gera codigo autimático
	@Column(name="codigo_disciplina")
	private Integer codigo;
	
	@Column(name="nome_disciplina", length=50, nullable=false)
	private String nome;
	@Column(name="diasHorarios__disciplina", length=50, nullable=true)
	private String diasHorarios;
	@Column(name="semestre__disciplina", nullable=false)
	private String semestre;
	
	@ManyToOne
	@JoinColumn(name="codigo_professor", referencedColumnName="codigo_professor")
	private ProfessorModel professorModel;

	public DisciplinaModel(){
		
	}
	
	public DisciplinaModel(Integer codigo, String nome, String diasHorarios, String semestre, ProfessorModel professor) {
		this.codigo = codigo;
		this.nome = nome;
		this.diasHorarios = diasHorarios;
		this.semestre = semestre;
		this.professorModel = professor;
	}

	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDiasHorarios() {
		return diasHorarios;
	}
	public void setDiasHorarios(String diasHorarios) {
		this.diasHorarios = diasHorarios;
	}
	public String getSemestre() {
		return semestre;
	}
	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	public ProfessorModel getProfessor() {
		return professorModel;
	}
	public void setProfessor(ProfessorModel professor) {
		this.professorModel = professor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisciplinaModel other = (DisciplinaModel) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
