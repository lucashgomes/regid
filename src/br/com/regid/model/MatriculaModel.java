package br.com.regid.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement
@Entity
@Table(name="matricula")
public class MatriculaModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="codigo_matricula")
	private Integer codigo;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="codigo_aluno", referencedColumnName="codigo_aluno"), 
		@JoinColumn(name="prontuario_aluno", referencedColumnName="prontuario_aluno")
	})
	private AlunoModel alunoModel;

	@ManyToMany
	@JoinColumn(name="codigo_disciplina", referencedColumnName="codigo_disciplina")
	private List<DisciplinaModel> disciplinaModel;
	
	@Column(name="semestre")
	private String semestre;

	@Column(name="status")
	private String status;

	public MatriculaModel(){
		
	}
	
	public MatriculaModel(Integer codigo,AlunoModel alunoModel, DisciplinaModel disciplinaModel, String semestre, 
			String status) {
		this.codigo = codigo;
		this.alunoModel = alunoModel;
		this.disciplinaModel = (List<DisciplinaModel>) disciplinaModel;
		this.semestre = semestre;
		this.status = status;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public AlunoModel getAlunoModel() {
		return alunoModel;
	}

	public void setAlunoModel(AlunoModel alunoModel) {
		this.alunoModel = alunoModel;
	}

	public void setDisciplinaModel(List<DisciplinaModel> disciplinaModel) {
		this.disciplinaModel = disciplinaModel;
	}
	
	@XmlTransient
	public List<DisciplinaModel> getDisciplinas() {
		return disciplinaModel;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatriculaModel other = (MatriculaModel) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
