package br.com.regid.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@Entity
@Table(name="periodoMatricula")
public class PeriodoMatriculaModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="codigo_periodoMatricula")
	private Integer codigo;
	
	@Column(name="periodo_matricula_data_inicio",  nullable=true)
	private Date periodoMatriculaDataInicio;
	
	@Column(name="periodo_matricula_data_fim",  nullable=true)
	private Date periodoMatriculaDataFim;
	
	public PeriodoMatriculaModel(Integer codigo, Date periodoMatriculaDataInicio, Date periodoMatriculaDataFim) {
		this.codigo = codigo;
		this.periodoMatriculaDataInicio = periodoMatriculaDataInicio;
		this.periodoMatriculaDataFim = periodoMatriculaDataFim;
	}

	public PeriodoMatriculaModel(){
		
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Date getPeriodoMatriculaDataInicio() {
		return periodoMatriculaDataInicio;
	}

	public void setPeriodoMatriculaDataInicio(Date periodoMatriculaDataInicio) {
		this.periodoMatriculaDataInicio = periodoMatriculaDataInicio;
	}

	public Date getPeriodoMatriculaDataFim() {
		return periodoMatriculaDataFim;
	}

	public void setPeriodoMatriculaDataFim(Date periodoMatriculaDataFim) {
		this.periodoMatriculaDataFim = periodoMatriculaDataFim;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodoMatriculaModel other = (PeriodoMatriculaModel) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}