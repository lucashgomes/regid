package br.com.regid.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@Entity
@Table(name="professor")
public class ProfessorModel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="codigo_professor")
	private Integer codigo;
	
	@Column(name="nome_professor", length=50, nullable=false)
	private String nome;
	
	@Column(name="email_professor", length=50, nullable=true)
	private String email;
	
	@Column(name="telefone_professor", length=15, nullable=false)
	private String telefone;
	
	@Column(name="titulacao_professor", length=15, nullable=false)
	private String titulacao;
	
	public void setDisciplinaModel(List<DisciplinaModel> disciplinaModel) {
		this.disciplinaModel = disciplinaModel;
	}
	
	@OneToMany(mappedBy="professorModel")
	private List<DisciplinaModel> disciplinaModel;
	
	@XmlTransient
	public List<DisciplinaModel> getDisciplinas() {
		return disciplinaModel;
	}
	
	public ProfessorModel(){
		
	}
	
	public ProfessorModel(Integer codigo, String nome, String email, String telefone, String titulacao) {
		this.codigo = codigo;
		this.nome = nome;
		this.email = email;
		this.telefone = telefone;
		this.titulacao = titulacao;
	}

	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getTitulacao() {
		return titulacao;
	}
	public void setTitulacao(String titulacao) {
		this.titulacao = titulacao;
	}
	
	/**
	 * O HashCode é utilizado para comparar um parametro passado com os listados no hash. O ideal é ter apenas o código
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorModel other = (ProfessorModel) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
