package br.com.regid.model.rest.facade;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.regid.model.MatriculaModel;
import br.com.regid.model.service.MatriculaServiceInterface;

@Path("/matriculas")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes(MediaType.APPLICATION_JSON)
public class MatriculaFacade {
	
	@Inject
	private MatriculaServiceInterface matriculaServiceInterface; 
	
	@GET
	public List<MatriculaModel> getMatriculas() {
		return matriculaServiceInterface.getMatriculas();
	}
	
	@POST
	public MatriculaModel salvarMatricula(MatriculaModel matriculaModel) {
		return matriculaServiceInterface.salvarMatricula(matriculaModel);
	}
	
	@PUT
	public void atualizar(MatriculaModel matriculaModel) {
		matriculaServiceInterface.alterar(matriculaModel);
	}
	
	@DELETE
	@Path("/{codigoMatricula}")
	public void excluir(@PathParam("codigoMatricula") Integer codigoMatricula) {
		MatriculaModel matriculaModel = new MatriculaModel();
		matriculaModel.setCodigo(codigoMatricula);
		matriculaServiceInterface.excluir(matriculaModel);
		
	}
}
