package br.com.regid.model.rest.facade;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.regid.model.AlunoModel;
import br.com.regid.model.service.AlunoServiceInterface;

@Path("/alunos")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes(MediaType.APPLICATION_JSON)
public class AlunoFacade {

	@Inject
	private AlunoServiceInterface alunoServiceInterface; 
	
	@GET
	public List<AlunoModel> getAlunos() {
		return alunoServiceInterface.getAlunos();
	}
	
	@POST
	public AlunoModel salvarAluno(AlunoModel alunoModel) {
		return alunoServiceInterface.salvarAluno(alunoModel);
	}
	
	@PUT
	public void atualizar(AlunoModel alunoModel) {
		alunoServiceInterface.alterar(alunoModel);
	}
	
	@DELETE
	@Path("/{codigoAluno}")
	public void excluir(@PathParam("codigoAluno") Integer codigoAluno) {
		AlunoModel alunoModel = new AlunoModel();
		alunoModel.setCodigo(codigoAluno);
		alunoServiceInterface.excluir(alunoModel);
		
	}
	
}
