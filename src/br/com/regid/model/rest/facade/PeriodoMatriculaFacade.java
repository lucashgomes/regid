package br.com.regid.model.rest.facade;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.regid.model.PeriodoMatriculaModel;
import br.com.regid.model.service.PeriodoMatriculaServiceInterface;

@Path("/periodosMatricula")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes(MediaType.APPLICATION_JSON)
public class PeriodoMatriculaFacade {

	@Inject
	private PeriodoMatriculaServiceInterface periodoMatriculaServiceInterface; 
	
	@GET
	public List<PeriodoMatriculaModel> getPeriodosMatricula() {
		return periodoMatriculaServiceInterface.getPeriodosMatricula();
	}
	
	@POST
	public PeriodoMatriculaModel salvarPeriodoMatricula(PeriodoMatriculaModel periodoMatriculaModel) {
		return periodoMatriculaServiceInterface.salvarPeriodoMatricula(periodoMatriculaModel);
	}
	
	@PUT
	public void atualizar(PeriodoMatriculaModel periodoMatriculaModel) {
		periodoMatriculaServiceInterface.alterar(periodoMatriculaModel);
	}
	
	@DELETE
	@Path("/{codigoPeriodoMatricula}")
	public void excluir(@PathParam("codigoPeriodoMatricula") Integer codigoPeriodoMatricula) {
		PeriodoMatriculaModel periodoMatriculaModel = new PeriodoMatriculaModel();
		periodoMatriculaModel.setCodigo(codigoPeriodoMatricula);
		periodoMatriculaServiceInterface.excluir(periodoMatriculaModel);
		
	}
	
}
