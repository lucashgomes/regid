package br.com.regid.model.rest.facade;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.regid.model.DisciplinaModel;
import br.com.regid.model.service.DisciplinaServiceInterface;

@Path("/disciplinas")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes(MediaType.APPLICATION_JSON)
public class DisciplinaFacade {
	
	@Inject
	private DisciplinaServiceInterface disciplinaServiceInterface; 
	
	@GET
	public List<DisciplinaModel> getDisciplinas() {
		return disciplinaServiceInterface.getDisciplinas();
	}
	
	@POST
	public DisciplinaModel salvarDisciplina(DisciplinaModel disciplinaModel) {
		return disciplinaServiceInterface.salvarDisciplina(disciplinaModel);
	}
	
	@PUT
	public void atualizar(DisciplinaModel disciplinaModel) {
		disciplinaServiceInterface.alterar(disciplinaModel);
	}
	
	@DELETE
	@Path("/{codigoDisciplina}")
	public void excluir(@PathParam("codigoDisciplina") Integer codigoDisciplina) {
		DisciplinaModel disciplinaModel = new DisciplinaModel();
		disciplinaModel.setCodigo(codigoDisciplina);
		disciplinaServiceInterface.excluir(disciplinaModel);
		
	}

}
