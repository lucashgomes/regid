package br.com.regid.model.service;

import java.util.List;

import br.com.regid.model.ProfessorModel;

public interface ProfessorServiceInterface {

	ProfessorModel salvarProfessor(ProfessorModel professorModel);

	void alterar(ProfessorModel professorModel);

	void excluir(ProfessorModel professorModel);

	List<ProfessorModel> getProfessores();
}
