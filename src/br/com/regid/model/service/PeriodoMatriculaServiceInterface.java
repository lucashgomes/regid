package br.com.regid.model.service;

import java.util.List;

import br.com.regid.model.PeriodoMatriculaModel;

public interface PeriodoMatriculaServiceInterface {

	PeriodoMatriculaModel salvarPeriodoMatricula(PeriodoMatriculaModel periodoMatriculaModel);

	void alterar(PeriodoMatriculaModel periodoMatriculaModel);

	void excluir(PeriodoMatriculaModel periodoMatriculaModel);

	List<PeriodoMatriculaModel> getPeriodosMatricula();
}
