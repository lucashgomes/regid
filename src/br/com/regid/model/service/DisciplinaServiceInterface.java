package br.com.regid.model.service;

import java.util.List;

import br.com.regid.model.DisciplinaModel;

public interface DisciplinaServiceInterface {

	DisciplinaModel salvarDisciplina(DisciplinaModel disciplinaModel);

	void alterar(DisciplinaModel disciplinaModel);

	void excluir(DisciplinaModel disciplinaModel);

	List<DisciplinaModel> getDisciplinas();
	
}
