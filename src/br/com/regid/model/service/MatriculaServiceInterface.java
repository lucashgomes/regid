package br.com.regid.model.service;

import java.util.List;

import br.com.regid.model.MatriculaModel;

public interface MatriculaServiceInterface {
	
	MatriculaModel salvarMatricula(MatriculaModel matriculaModel);

	void alterar(MatriculaModel matriculaModel);

	void excluir(MatriculaModel matriculaModel);

	List<MatriculaModel> getMatriculas();

}
