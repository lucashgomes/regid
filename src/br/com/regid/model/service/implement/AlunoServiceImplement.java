package br.com.regid.model.service.implement;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import br.com.regid.dao.AlunoDAO;
import br.com.regid.model.AlunoModel;
import br.com.regid.model.service.AlunoServiceInterface;

public class AlunoServiceImplement implements AlunoServiceInterface{
	
	@Inject 
	private AlunoDAO alunoDAO;
	
	@Override
	public List<AlunoModel> getAlunos() {
		return alunoDAO.getAlunos();
	}
	
	@Override
	@Transactional
	public AlunoModel salvarAluno(AlunoModel alunoModel) {
		return alunoDAO.salvarAluno(alunoModel);
	}

	@Override
	@Transactional
	public void alterar(AlunoModel alunoModel) {
		alunoDAO.alterar(alunoModel);
		
	}

	@Override
	@Transactional
	public void excluir(AlunoModel alunoModel) {
		alunoDAO.excluir(alunoModel);
		
	}
}
