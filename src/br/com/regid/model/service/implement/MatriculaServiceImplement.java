package br.com.regid.model.service.implement;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import br.com.regid.dao.MatriculaDAO;
import br.com.regid.model.MatriculaModel;
import br.com.regid.model.service.MatriculaServiceInterface;

public class MatriculaServiceImplement implements MatriculaServiceInterface {

	@Inject 
	private MatriculaDAO matriculaDAO;
	
	@Override
	public List<MatriculaModel> getMatriculas() {
		return matriculaDAO.getMatriculas();
	}
	
	@Override
	@Transactional
	public MatriculaModel salvarMatricula(MatriculaModel matriculaModel) {
		return matriculaDAO.salvarMatricula(matriculaModel);
	}

	@Override
	@Transactional
	public void alterar(MatriculaModel matriculaModel) {
		matriculaDAO.alterar(matriculaModel);
		
	}

	@Override
	@Transactional
	public void excluir(MatriculaModel matriculaModel) {
		matriculaDAO.excluir(matriculaModel);
		
	}
}
