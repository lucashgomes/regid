package br.com.regid.model.service.implement;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import br.com.regid.dao.DisciplinaDAO;
import br.com.regid.model.DisciplinaModel;
import br.com.regid.model.service.DisciplinaServiceInterface;

public class DisciplinaServiceImplement implements DisciplinaServiceInterface{

	@Inject 
	private DisciplinaDAO disciplinaDAO;
	
	@Override
	public List<DisciplinaModel> getDisciplinas() {
		return disciplinaDAO.getDisciplinas();
	}
	
	@Override
	@Transactional
	public DisciplinaModel salvarDisciplina(DisciplinaModel disciplinaModel) {
		return disciplinaDAO.salvarDisciplina(disciplinaModel);
	}

	@Override
	@Transactional
	public void alterar(DisciplinaModel disciplinaModel) {
		disciplinaDAO.alterar(disciplinaModel);
		
	}

	@Override
	@Transactional
	public void excluir(DisciplinaModel disciplinaModel) {
		disciplinaDAO.excluir(disciplinaModel);
		
	}

}
