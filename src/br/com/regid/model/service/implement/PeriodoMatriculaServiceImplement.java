package br.com.regid.model.service.implement;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import br.com.regid.dao.PeriodoMatriculaDAO;
import br.com.regid.model.PeriodoMatriculaModel;
import br.com.regid.model.service.PeriodoMatriculaServiceInterface;

public class PeriodoMatriculaServiceImplement implements PeriodoMatriculaServiceInterface{
	
	@Inject 
	private PeriodoMatriculaDAO periodoMatriculaDAO;
	
	@Override
	public List<PeriodoMatriculaModel> getPeriodosMatricula() {
		return periodoMatriculaDAO.getPeriodosMatricula();
	}
	
	@Override
	@Transactional
	public PeriodoMatriculaModel salvarPeriodoMatricula(PeriodoMatriculaModel periodoMatriculaModel) {
		return periodoMatriculaDAO.salvarPeriodoMatricula(periodoMatriculaModel);
	}

	@Override
	@Transactional
	public void alterar(PeriodoMatriculaModel periodoMatriculaModel) {
		periodoMatriculaDAO.alterar(periodoMatriculaModel);
		
	}

	@Override
	@Transactional
	public void excluir(PeriodoMatriculaModel periodoMatriculaModel) {
		periodoMatriculaDAO.excluir(periodoMatriculaModel);
		
	}
}
