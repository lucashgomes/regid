package br.com.regid.model.service.implement;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import br.com.regid.dao.ProfessorDAO;
import br.com.regid.model.ProfessorModel;
import br.com.regid.model.service.ProfessorServiceInterface;

public class ProfessorServiceImplement implements ProfessorServiceInterface{

	@Inject 
	private ProfessorDAO professorDAO;
	
	@Override
	public List<ProfessorModel> getProfessores() {
		return professorDAO.getProfessores();
	}
	
	@Override
	@Transactional
	public ProfessorModel salvarProfessor(ProfessorModel professorModel) {
		return professorDAO.salvarProfessor(professorModel);
	}

	@Override
	@Transactional
	public void alterar(ProfessorModel professorModel) {
		professorDAO.alterar(professorModel);
		
	}

	@Override
	@Transactional
	public void excluir(ProfessorModel professorModel) {
		professorDAO.excluir(professorModel);
		
	}

}
