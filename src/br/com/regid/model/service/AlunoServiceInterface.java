package br.com.regid.model.service;

import java.util.List;

import br.com.regid.model.AlunoModel;

public interface AlunoServiceInterface {

	AlunoModel salvarAluno(AlunoModel alunoModel);

	void alterar(AlunoModel alunoModel);

	void excluir(AlunoModel alunoModel);

	List<AlunoModel> getAlunos();
}
