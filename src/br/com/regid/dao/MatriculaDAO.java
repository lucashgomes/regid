package br.com.regid.dao;

import java.util.List;

import br.com.regid.model.MatriculaModel;

public interface MatriculaDAO {

	MatriculaModel salvarMatricula(MatriculaModel matriculaModel);

	void alterar(MatriculaModel matriculaModel);

	void excluir(MatriculaModel matriculaModel);

	List<MatriculaModel> getMatriculas();
}
