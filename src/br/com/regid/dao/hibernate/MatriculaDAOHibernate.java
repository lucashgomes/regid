package br.com.regid.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.regid.dao.MatriculaDAO;
import br.com.regid.model.MatriculaModel;

public class MatriculaDAOHibernate implements MatriculaDAO {
	
	@PersistenceContext(unitName="RegIDPersistenceUnit")
	private EntityManager entityManager;
	
	@Override
	public MatriculaModel salvarMatricula(MatriculaModel matriculaModel) {
		entityManager.persist(matriculaModel);
		return matriculaModel;
	}

	@Override
	public void alterar(MatriculaModel matriculaModel) {
		MatriculaModel MatriculaModelMerge = entityManager.merge(matriculaModel);
		entityManager.persist(MatriculaModelMerge);
		
	}

	@Override
	public void excluir(MatriculaModel matriculaModel) {
		MatriculaModel MatriculaModelMerge = entityManager.merge(matriculaModel);
		entityManager.remove(MatriculaModelMerge);
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MatriculaModel> getMatriculas() {
	    Query query = entityManager.createQuery("from MatriculaModel");
	    return query.getResultList();
	}
}
