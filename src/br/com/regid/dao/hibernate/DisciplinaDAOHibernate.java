package br.com.regid.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.regid.dao.DisciplinaDAO;
import br.com.regid.model.DisciplinaModel;

public class DisciplinaDAOHibernate implements DisciplinaDAO {

	@PersistenceContext(unitName="RegIDPersistenceUnit")
	private EntityManager entityManager;
	
	//disciplina Neri
	@Override
	public DisciplinaModel salvarDisciplina(DisciplinaModel disciplinaModel) {
		entityManager.persist(disciplinaModel);
		return disciplinaModel;
	}

	@Override
	public void alterar(DisciplinaModel disciplinaModel) {
		DisciplinaModel disciplinaModelMerge = entityManager.merge(disciplinaModel);
		entityManager.persist(disciplinaModelMerge);
		
	}

	@Override
	public void excluir(DisciplinaModel disciplinaModel) {
		DisciplinaModel disciplinaModelMerge = entityManager.merge(disciplinaModel);
		entityManager.remove(disciplinaModelMerge);
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DisciplinaModel> getDisciplinas() {
	    Query query = entityManager.createQuery("from DisciplinaModel");
	    return query.getResultList();
	}
}
