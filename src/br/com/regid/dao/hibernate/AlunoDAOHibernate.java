package br.com.regid.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.regid.dao.AlunoDAO;
import br.com.regid.model.AlunoModel;

public class AlunoDAOHibernate implements AlunoDAO {

	@PersistenceContext(unitName="RegIDPersistenceUnit")
	private EntityManager entityManager;
	
	@Override
	public AlunoModel salvarAluno(AlunoModel alunoModel) {
		entityManager.persist(alunoModel);
		return alunoModel;
	}

	@Override
	public void alterar(AlunoModel alunoModel) {
		AlunoModel AlunoModelMerge = entityManager.merge(alunoModel);
		entityManager.persist(AlunoModelMerge);
		
	}

	@Override
	public void excluir(AlunoModel alunoModel) {
		AlunoModel AlunoModelMerge = entityManager.merge(alunoModel);
		entityManager.remove(AlunoModelMerge);
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AlunoModel> getAlunos() {
	    Query query = entityManager.createQuery("from AlunoModel");
	    return query.getResultList();
	}
}
