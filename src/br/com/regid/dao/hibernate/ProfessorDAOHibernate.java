package br.com.regid.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.regid.dao.ProfessorDAO;
import br.com.regid.model.ProfessorModel;

public class ProfessorDAOHibernate implements ProfessorDAO {

	@PersistenceContext(unitName = "RegIDPersistenceUnit")
	private EntityManager entityManager;

	@Override
	public ProfessorModel salvarProfessor(ProfessorModel professorModel) {
		entityManager.persist(professorModel);
		return professorModel;
	}

	@Override
	public void alterar(ProfessorModel professorModel) {
		ProfessorModel professorModelMerge = entityManager.merge(professorModel);
		entityManager.persist(professorModelMerge);
	}

	@Override
	public void excluir(ProfessorModel professorModel) {
		ProfessorModel professorModelMerge = entityManager.merge(professorModel);
		entityManager.remove(professorModelMerge);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ProfessorModel> getProfessores() {
		Query query = entityManager.createQuery("from ProfessorModel");
		return query.getResultList();
	}
}
