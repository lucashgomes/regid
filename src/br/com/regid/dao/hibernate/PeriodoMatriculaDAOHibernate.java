package br.com.regid.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.regid.dao.PeriodoMatriculaDAO;
import br.com.regid.model.PeriodoMatriculaModel;

public class PeriodoMatriculaDAOHibernate implements PeriodoMatriculaDAO {

	@PersistenceContext(unitName="RegIDPersistenceUnit")
	private EntityManager entityManager;
	
	@Override
	public PeriodoMatriculaModel salvarPeriodoMatricula(PeriodoMatriculaModel periodoMatriculaModel) {
		entityManager.persist(periodoMatriculaModel);
		return periodoMatriculaModel;
	}

	@Override
	public void alterar(PeriodoMatriculaModel periodoMatriculaModel) {
		PeriodoMatriculaModel PeriodoMatriculaModelMerge = entityManager.merge(periodoMatriculaModel);
		entityManager.persist(PeriodoMatriculaModelMerge);
		
	}

	@Override
	public void excluir(PeriodoMatriculaModel periodoMatriculaModel) {
		PeriodoMatriculaModel PeriodoMatriculaModelMerge = entityManager.merge(periodoMatriculaModel);
		entityManager.remove(PeriodoMatriculaModelMerge);
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<PeriodoMatriculaModel> getPeriodosMatricula() {
	    Query query = entityManager.createQuery("from PeriodoMatriculaModel");
	    return query.getResultList();
	}
}
