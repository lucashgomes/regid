package br.com.regid.dao;

import java.util.List;

import br.com.regid.model.AlunoModel;

public interface AlunoDAO {

	AlunoModel salvarAluno(AlunoModel alunoModel);

	void alterar(AlunoModel alunoModel);

	void excluir(AlunoModel alunoModel);

	List<AlunoModel> getAlunos();
}
