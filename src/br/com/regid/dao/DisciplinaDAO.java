package br.com.regid.dao;

import java.util.List;

import br.com.regid.model.DisciplinaModel;

public interface DisciplinaDAO {

	DisciplinaModel salvarDisciplina(DisciplinaModel disciplinaModel);

	void alterar(DisciplinaModel disciplinaModel);

	void excluir(DisciplinaModel disciplinaModel);

	List<DisciplinaModel> getDisciplinas();
}
