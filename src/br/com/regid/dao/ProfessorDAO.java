package br.com.regid.dao;

import java.util.List;

import br.com.regid.model.ProfessorModel;

public interface ProfessorDAO {

	ProfessorModel salvarProfessor(ProfessorModel professorModel);

	void alterar(ProfessorModel professorModel);

	void excluir(ProfessorModel professorModel);

	List<ProfessorModel> getProfessores();
}
