var alunosModulo = angular.module('alunosModulo', []);

alunosModulo.controller('alunosController', function($scope, $http) {

	urlAluno = 'http://localhost:8080/RegID/rest/alunos';

	$scope.listaAlunos = function() {
		$http({
			method: 'GET',
			url: urlAluno
		}).then(function (alunos) {
			$scope.alunos = alunos.data;
		}, function (error) {
			alert("Houve o erro: " + error);
		});
	}

	$scope.selecionaAluno = function(alunoSelecionado) {
		$scope.aluno = alunoSelecionado;
	}
	
	$scope.limparCampos = function() {
		$scope.aluno = null;
	}
	
	$scope.salvar = function(){
		if($scope.aluno.codigo == undefined){
			$http({
				method: 'POST',
				url: urlAluno,
				data: $scope.aluno
			}).then(function (aluno) {
			//	$scope.listaAlunos.push($scope.aluno);
				$scope.limparCampos();
				$scope.listaAlunos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});		
		}else{
			$http({
				method: 'PUT',
				url: urlAluno,
				data: $scope.aluno
			}).then(function (aluno) {
				$scope.listaAlunos();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});	
		}
	}
	
	$scope.excluir = function(){
		if($scope.aluno.codigo == undefined){
			alert("Selecione um registro para a exclusão!");
		}else{
			$http({
				method: 'DELETE',
				url: urlAluno + '/' + $scope.aluno.codigo
			}).then(function (aluno) {
				$scope.listaAlunos();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});
		}
	}

	$scope.listaAlunos();
});