var periodosMatriculaModulo = angular.module('periodosMatriculaModulo', []);

periodosMatriculaModulo.controller('periodosMatriculaController', function($scope, $http) {

	urlPeriodoMatricula = 'http://localhost:8080/RegID/rest/periodosMatricula';

	$scope.listaPeriodosMatricula = function() {
		$http({
			method: 'GET',
			url: urlPeriodoMatricula
		}).then(function (periodosMatricula) {
			$scope.periodosMatricula = periodosMatricula.data;
		}, function (error) {
			alert("Houve o erro: " + error);
		});
	}

	$scope.selecionaPeriodoMatricula = function(periodoMatriculaSelecionado) {
		$scope.periodoMatricula = periodoMatriculaSelecionado;
	}
	
	$scope.limparCampos = function() {
		$scope.periodoMatricula = null;
	}
	
	$scope.salvar = function(){
		if($scope.periodoMatricula.codigo == undefined){
			$http({
				method: 'POST',
				url: urlPeriodoMatricula,
				data: $scope.periodoMatricula
			}).then(function (periodoMatricula) {
			//	$scope.listaPeriodosMatricula.push($scope.periodoMatricula);
				$scope.limparCampos();
				$scope.listaPeriodosMatricula();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});		
		}else{
			$http({
				method: 'PUT',
				url: urlPeriodoMatricula,
				data: $scope.periodoMatricula
			}).then(function (periodoMatricula) {
				$scope.listaPeriodosMatricula();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});	
		}
	}
	
	$scope.excluir = function(){
		if($scope.periodoMatricula.codigo == undefined){
			alert("Selecione um registro para a exclusão!");
		}else{
			$http({
				method: 'DELETE',
				url: urlPeriodoMatricula + '/' + $scope.periodoMatricula.codigo
			}).then(function (periodoMatricula) {
				$scope.listaPeriodosMatricula();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});
		}
	}

	$scope.listaPeriodosMatricula();
});