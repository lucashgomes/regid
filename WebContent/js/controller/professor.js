var professoresModulo = angular.module('professoresModulo', []);

professoresModulo.controller('professoresController', function($scope, $http) {

	urlProfessor = 'http://localhost:8080/RegID/rest/professores';

	$scope.listaProfessores = function() {
		$http({
			method: 'GET',
			url: urlProfessor
		}).then(function (professores) {
			$scope.professores = professores.data;
		}, function (error) {
			alert("Houve o erro: " + error);
		});
	}

	$scope.selecionaProfessor = function(professorSelecionado) {
		$scope.professor = professorSelecionado;
	}
	
	$scope.limparCampos = function() {
		$scope.professor = null;
	}
	
	$scope.salvar = function(){
		if($scope.professor.codigo == undefined){
			$http({
				method: 'POST',
				url: urlProfessor,
				data: $scope.professor
			}).then(function (professor) {
			//	$scope.listaProfessores.push($scope.professor);
				$scope.limparCampos();
				$scope.listaProfessores();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});		
		}else{
			$http({
				method: 'PUT',
				url: urlProfessor,
				data: $scope.professor
			}).then(function (professor) {
				$scope.listaProfessores();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});	
		}
	}
	
	$scope.excluir = function(){
		if($scope.professor.codigo == undefined){
			alert("Selecione um registro para a exclusão!");
		}else{
			$http({
				method: 'DELETE',
				url: urlProfessor + '/' + $scope.professor.codigo
			}).then(function (professor) {
				$scope.listaProfessores();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});
		}
	}

	$scope.listaProfessores();
});