var disciplinasModulo = angular.module('disciplinasModulo', []);

disciplinasModulo.controller('disciplinasController', function($scope, $http) {

	
	urlDisciplina = 'http://localhost:8080/RegID/rest/disciplinas';
	urlProfessor = 'http://localhost:8080/RegID/rest/professores';

	$scope.listaProfessores = function() {
		$http({
			method: 'GET',
			url: urlProfessor
		}).then(function (professores) {
			$scope.professores = professores.data;
		}, function (error) {
			alert("Houve o erro: " + error);
		});
	}
	$scope.listaDisciplinas = function() {
		$http({
			method: 'GET',
			url: urlDisciplina
		}).then(function (disciplinas) {
			$scope.disciplinas = disciplinas.data;
		}, function (error) {
//			alert("Houve o erro: " + error);
		});
	}

	$scope.selecionaDisciplina = function(disciplinaSelecionado) {
		$scope.disciplina = disciplinaSelecionado;
	}
	
	$scope.limparCampos = function() {
		$scope.disciplina = null;
	}
	
	$scope.salvar = function(){
		if($scope.disciplina.codigo == undefined){
			$http({
				method: 'POST',
				url: urlDisciplina,
				data: $scope.disciplina
			}).then(function (disciplina) {
//				$scope.listaDisciplinas.push($scope.disciplina);
				$scope.limparCampos();
				$scope.listaDisciplinas();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});		
		}else{
			$http({
				method: 'PUT',
				url: urlDisciplina,
				data: $scope.disciplina
			}).then(function (disciplina) {
				$scope.listaDisciplinas();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});	
		}
	}
	
	$scope.excluir = function(){
		if($scope.disciplina.codigo == undefined){
			alert("Selecione um registro para a exclusão!");
		}else{
			$http({
				method: 'DELETE',
				url: urlDisciplina + '/' + $scope.disciplina.codigo
			}).then(function (disciplina) {
				$scope.listaDisciplinas();
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});
		}
	}

	$scope.listaProfessores();
	$scope.listaDisciplinas();
});