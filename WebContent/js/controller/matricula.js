var matriculaModulo = angular.module('matriculaModulo', []);

matriculaModulo.controller("matriculaController", function($scope, $http) {

	urlMatricula = 'http://localhost:8080/RegID/rest/matriculas';
    urlDisciplina = 'http://localhost:8080/RegID/rest/disciplinas';
    urlAluno = 'http://localhost:8080/RegID/rest/alunos';
    
	$scope.listaDisciplinas = function() {
		$http({
			method: 'GET',
			url: urlDisciplina
		}).then(function (disciplinas) {
			$scope.disciplinas = disciplinas.data;
		}, function (error) {
			console.log("Erro ao listar: " + error);
		});
	}
	
	$scope.listarAlunos = function() {
		$http({
			method: 'GET',
			url: urlAluno
		}).then(function (alunos) {
			$scope.alunos = alunos.data;
		}, function (error) {
			console.log("Erro ao listar: " + error);
		});
	}
	
	$scope.limparCampos = function() {
        $scope.matricula = "";
    }
	
	$scope.salvar = function() {
		if ($scope.matricula.codigo == undefined) {
			$http({
				method: 'POST',
				url: urlMatricula,
				data: $scope.matricula
			}).then(function (matricula) {
			//	$scope.listaProfessores.push($scope.professor);
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});	
		} else {
			$http({
				method: 'PUT',
				url: urlMatricula,
				data: $scope.matricula
			}).then(function (matricula) {
			//	$scope.listaProfessores.push($scope.professor);
				$scope.limparCampos();
			}, function(error){
				alert("Erro ao salvar: " + error);
			});	
		}
	}
	
//	$scope.matricula.prontuario = $scope.alunos.prontuario;
//	$scope.matricula.nome = $scope.alunos.nome;
	$scope.listaDisciplinas();  
});

//Compara se há parente em outro select e exibe os relacionados
matriculaModulo.filter('filterBySemestre', function () {
    return function (matriculaDisciplina, matriculaSemestre) {
        var filtered = [];
        if (matriculaSemestre === null) {
            return filtered;
        }
        angular.forEach(matriculaDisciplina, function (matDisciplina) {
            if (matDisciplina.semestre == matriculaSemestre) {
                filtered.push(matDisciplina);
            }
        });
        return filtered;
    };
});